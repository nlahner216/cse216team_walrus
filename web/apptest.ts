var describe: any;
var it: any;
var expect: any;
var $: any;
var spyOn:any;
var spy: any;
var jasmine: any;
var getSampleResponse: any;

describe("Tests of basic math functions", function() {
    
    it("Adding 1 should work", function() {
        var foo = 0;
        foo += 1;
        expect(foo).toEqual(1);
    });

    it("Subtracting 1 should work", function () {
        var foo = 0;
        foo -= 1;
        expect(foo).toEqual(-1);
    });

    it("UI Test: Add Button Hides Listing", function(){
        // click the button for showing the add button
        $('#showFormButton').click();
        // expect that the add form is not hidden
        expect($("#addElement").attr("style").indexOf("display: none;")).toEqual(-1);
        // expect tha tthe element listing is hidden
        expect($("#showElements").attr("style").indexOf("display: none;")).toEqual(0);
        // reset the UI, so we don't mess up the next test
        $('#addCancel').click();        
    });

    it("UI Test: Entering Null Title Gives Error", function(){
        
        //set value of the title
        $('#newTitle').val("");
        //click on the add button
        $('#addButton').click();
        //create a spy to check for alert
        spyOn(window, 'alert');
        //expect alert to pop up
        expect(window.alert);
        // reset the UI, so we don't mess up the next test
        $('#addCancel').click();
        spy.reset();

    })
//#TODO: figure out how to have multiple tests with spies in them
/* 
    it("UI Test: Entering valid Title and Message works", function(){
        var spyOn = 0;
        //set value of the title
        $('#newTitle').val("Title");
        //set value of the message
        $('#newMessage').val("Message");
        //click on the add button
        $('#addButton').click();
        spyOn($, "ajax").and.callFake(function(options) {
            options.success();
        });
        var callback = jasmine.createSpy();
        getSampleResponse( callback);
        expect(callback).toHaveBeenCalled();

    })
*/
});