package eir220.cse216.lehigh.edu.phase0;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class StringSingleton {
    private volatile static StringSingleton obj;
    static Queue<String> queue = new LinkedList<String>();

    private StringSingleton(){}

    public static StringSingleton getInstance(){
        if(obj == null){
            synchronized (StringSingleton.class){
                if(obj == null)
                    obj = new StringSingleton();
            }
        }
        return obj;
    }

    public static void add (String toAdd){
        synchronized (queue){
            queue.add(toAdd);
        }
    }
}
